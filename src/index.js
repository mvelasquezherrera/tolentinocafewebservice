const express = require("express");
const chalk = require('chalk');
const app = express();
const morgan = require("morgan");
const mssql = require("mssql");
const environment=process.env.NODE_ENV || "production"
const config = require(`./database/database.${environment}`);

// settings
app.set("port", process.env.PORT || 8081);
app.set("json spaces", 2);

// middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

// routes
app.use("/api", require("./routes/home.routes"));
app.use("/api", require("./routes/login.routes"));
app.use("/api", require("./routes/menu.routes"));

// starting the server
console.log(chalk.blue(environment))
mssql.connect(config, (err, res) => {
  if (!err) {
    app.listen(app.get("port"), () => {
      console.log("Server en el puerto ", app.get("port"));
    });
  } else {
    console.log(err);
  }
});
