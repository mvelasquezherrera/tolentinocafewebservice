function capitlFrstLttr(text) {
    return text
      .toLowerCase()
      .split(' ')
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ');
  }
  
  /*function primeraLetra(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
  }*/
  
  module.exports = {
    capitlFrstLttr,
  };