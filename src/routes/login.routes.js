"use strict";

const {Router} = require("express");
const router = Router();
const service = require("../services");
const mssql = require("mssql");
const md5 = require("md5");
const nodemailer = require("nodemailer");
const fn = require("../extras/functions");
const emailHtml = require("../email/email");
const moment = require("moment");

router.post("/login", (req, res) => {
  const user = {
    usuario: req.body.usuario,
    contraseña: req.body.contraseña
  };

  if (user.usuario && user.contraseña) {
    var request = new mssql.Request();

    async function getInfoLogin() {
      console.log("user.usuario: ", user.usuario);
      res.json({
        token: await generateTokens(),
        user: await getUser()
      });
    }

    function generateTokens() {
      return new Promise(resolve => {
        
        let result = [];

        result.push({
          accessToken: service.createToken(user.usuario),  
          resfreshToken: ""
        });
        
        resolve(result[0]);
        
        resolve();
      });
    }

    function getUser() {
      return new Promise(resolve => {
        var request = new mssql.Request();
        request.input("usuario", mssql.VarChar, user.usuario);
        request.execute("PROY_TOLENTINO_PERSONA_INFO_SELECT", (err, rows) => {
          if (!err) {
            //console.log(rows.recordset);
            if (rows.recordset.length != 0) {
              let result = [];
              for (var i = 0; i < rows.recordset.length; i++) {
                //console.log(rows.recordset[i])
                result.push({
                  nombre1:fn.capitlFrstLttr(rows.recordset[i].nombre1),
                  nombre2:fn.capitlFrstLttr(rows.recordset[i].nombre2),
                  // nombre3:fn.capitlFrstLttr(rows.recordset[i].nombre3),
                  apellidoPaterno:fn.capitlFrstLttr(rows.recordset[i].apellidoPaterno),
                  apellidoMaterno:fn.capitlFrstLttr(rows.recordset[i].apellidoMaterno),
                  nombreCompleto:fn.capitlFrstLttr(rows.recordset[i].nombreCompleto),
                  celular: rows.recordset[i].celular,
                  correo: rows.recordset[i].correo
                });
              }
              resolve(result[0]);
            }
            resolve();
          } else {
            console.log(err);
          }
        });
      });
    }

    request.input("usuario", mssql.VarChar, user.usuario);
    request.query("SELECT idUsuario, contraseña, flag FROM Usuario WHERE idUsuario = @usuario", (err, rows) => {
      if (!err) {
        //Si el usuario existe en la base de Datos
        if (rows.rowsAffected > 0) {
          const userDb = rows.recordset[0];
          //Si tiene estado N, comparación sin encriptar
          console.log("Usuario con estado " + userDb.flag  );
            // if (userDb.contraseña == md5(user.clave)) {
            if (userDb.contraseña == user.contraseña) {
              getInfoLogin();
            } else {
              res.json({message: "Contraseña incorrecta"});
            }
          
        } else {
          res.status(404).send({message: "Usuario incorrecto"});
        }
      } else {
        console.log(err);
        res.status(500);
      }
    });

  } else {
    res.status(500).send({message: "Campos incompletos"});
  }
});

router.post("/cambiarContra", (req, res) => {
  const usuario = req.body.usuario;
  var clave = req.body.clave;
  var nuevaClave = req.body.nuevaClave;
  const codigo = req.body.codigo;
  if (usuario && nuevaClave && (clave || codigo)) {
    var request = new mssql.Request();
    request.input("usuario", mssql.VarChar, usuario);
    request.query("select estado,clave,SUBSTRING(TOKEN,0,7) 'codigo' from USUARIO where USUARIO=@usuario", (err, rows) => {
      if (!err) {
        const userDb = rows.recordset[0];
        //En caso de recuperacion de contraseña.
        if (codigo) {
          if (codigo.toUpperCase() == userDb.codigo.toUpperCase()) 
            ejecutarProcedure(userDb.clave);
          }
        else {
          //Si el usuario es activo la contraseña ingresada se encripta.
          if (userDb.estado == "A") {
            clave = md5(clave);
          } else {
            if (clave == userDb.clave) {
              ejecutarProcedure(clave);
            } else {
              res.sendStatus(404);
            }
          }
        }
      } else {
        console.log(err);
        res.sendStatus(500);
      }
    });
  } else {
    res.status(500).send({message: "Campos imcompletos"});
  }

  function ejecutarProcedure(clave) {
    var request = new mssql.Request();
    request.input("USUARIO", mssql.VarChar, usuario);
    request.input("CLAVE", mssql.VarChar, clave);
    request.input("CLAVE_NUEVA", mssql.VarChar, md5(nuevaClave));
    request.execute("CAMBIAR_CLAVE", (err, recordsets, returnValue) => {
      if (!err) {
        //console.log(recordsets.rowsAffected);
        res.sendStatus(200).send();
      } else {
        console.log(err);
        res.sendStatus(500);
      }
    });
  }
});

router.post("/recupContra", (req, res) => {
  const email = req.body.email
    ? req.body.email
    : "";
  const usuario = email.replace("@usmp.pe", "");
  var nombre = "";
  var request = new mssql.Request();
  request.input("usuario", mssql.VarChar, usuario);
  request.query("select NOMBRE1,APELLIDO_PATERNO from PERSONA where USUARIO=@usuario", (err, rows) => {
    if (!err) {
      if (rows.rowsAffected > 0) {
        const userDb = rows.recordset[0];
        nombre = fn.capitlFrstLttr(userDb.NOMBRE1.trim() + " " + userDb.APELLIDO_PATERNO.trim());
        generarToken();
      } else {
        res.status(404).send();
      }
    } else {
      console.log(err);
    }
  });

  function generarToken() {
    var request = new mssql.Request();
    request.input("USUARIO", mssql.VarChar, usuario);
    request.output("TOKEN");
    request.execute("GENERAR_TOKEN_USUARIO", (err, rows) => {
      if (!err) {
        var clave = rows.output.TOKEN.substring(0, 6).toUpperCase();
        enviarEmail(clave);
      } else {
        console.log(err);
      }
    });
  }

  function enviarEmail(clave) {
    var transporter = nodemailer.createTransport({
      service: "outlook",
      auth: {
        user: "mvelasquezherrera@outlook.com",
        pass: "marceloV19"
      }
    });

    var mailOptions = {
      from: '"MV" <mvelasquezherrera@outlook.com>',
      to: correo,
      subject: "Restablecer clave Tolentino Café",
      html: emailHtml.generarBody(nombre, clave)
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        res.json({
          message: "Email sent: " + info.response
        });
      }
    });
  }
});

router.post("/verifCod", (req, res) => {
  var request = new mssql.Request();
  request.input("usuario", mssql.VarChar, req.body.email.replace("@usmp.pe", ""));
  request.input("codigo", mssql.VarChar, req.body.codigo);
  request.query("select FORMAT(EXPIRACION_TOKEN,'yyyy-MM-dd HH:mm:ss') 'FEC_EXP' from USUARIO where USUARIO=@usuario and SUBSTRING(TOKEN,0,7) = @codigo", (err, rows) => {
    if (!err) {
      if (rows.rowsAffected > 0) {
        var fecExp = rows.recordset[0].FEC_EXP;
        if (moment().unix() < moment(fecExp).unix()) 
          res.sendStatus(200);
        else 
          res.sendStatus(403);
        }
      else 
        res.sendStatus(404);
      }
    else {
      console.log(err);
    }
  });
});

module.exports = router;