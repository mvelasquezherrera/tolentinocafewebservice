const {Router} = require("express");
const router = Router();
const auth = require("../middlewares/auth");
const mssql = require("mssql");
const fn = require("../extras/functions");
const moment = require("moment");


/////////////////////////////////////////////// CARTA ////////////////////////////////////////////////////
router.get('/carta', (req, res) => {
    var request = new mssql.Request();
    request.execute("PROY_TOLENTINO_LISTAR_CARTA", (err, rows) => {
        if (!err) {
            const result = rows.recordset;
            res.json(result);
        } else {
            console.log(err);
        }
    });
});

module.exports = router; 