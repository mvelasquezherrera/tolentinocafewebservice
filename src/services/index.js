const jwt = require('jwt-simple');
const moment = require('moment');

function createToken(user) {
  const payload = {
    sub: user,
    iat: moment().unix(),
    exp: moment()
      .add(3, 'months')
      .unix(),
  };
  return jwt.encode(payload, 'tokenKey');
}

function decodeToken(token) {
  const decoded = new Promise((resolve, reject) => {
    try {
      const payload = jwt.decode(token, 'tokenKey');
      resolve(payload.sub);
    } catch (err) {
      reject({
        status: 500,
        message: 'Invalid Token',
      });
    }
  });

  return decoded;
}

module.exports = {
  createToken,
  decodeToken,
};
